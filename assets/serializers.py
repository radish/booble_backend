from rest_framework import serializers

from assets.models import Asset


class AssetSerializer(serializers.ModelSerializer):
    class Meta:
        model = Asset
        fields = ('room', 'type', 'name', 'count',)
        read_only_fields = ('id',)
