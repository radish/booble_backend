from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.ListAssets.as_view()),
    url(r'^(?P<pk>[0-9]+)/$', views.AssetDetails.as_view()),
    url(r'^room/(?P<room>[0-9]+)/$', views.AssetsByRoom.as_view()),
]
