from django.db import models

from rooms.models import Room


class Asset(models.Model):
    room = models.ForeignKey(Room, on_delete=models.CASCADE)
    type = models.CharField(max_length=256)
    name = models.CharField(max_length=1024)
    count = models.PositiveIntegerField()

    def __str__(self):
        return '{} - {}'.format(self.room, self.name)
