from rest_framework import generics

from assets.models import Asset
from assets.serializers import AssetSerializer


class ListAssets(generics.ListAPIView):
    queryset = Asset.objects.all()
    serializer_class = AssetSerializer


class AssetDetails(generics.RetrieveAPIView):
    queryset = Asset.objects.all()
    serializer_class = AssetSerializer


class AssetsByRoom(generics.ListAPIView):
    serializer_class = AssetSerializer
    lookup_url_kwarg = 'room'

    def get_queryset(self):
        room = self.kwargs.get(self.lookup_url_kwarg)
        assets = Asset.objects.filter(room_id=room)
        return assets
