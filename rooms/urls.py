from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.ListRooms.as_view()),
    url(r'^(?P<pk>[0-9]+)/$', views.RoomDetails.as_view()),
    url(r'^reservations/?$', views.ReservationsList.as_view(),
        name='reservations-list'),
    url(r'^reservations/(?P<pk>[0-9]+)$', views.ReservationsDetails.as_view())
]
