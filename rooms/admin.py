from django.contrib import admin

from rooms.models import Room, Reservation, ReservationDateTimeRange


class ReservationDateTimeRangeInline(admin.TabularInline):
    model = ReservationDateTimeRange


class ReservationAdmin(admin.ModelAdmin):
    inlines = [
        ReservationDateTimeRangeInline,
    ]

admin.site.register(Room)
admin.site.register(Reservation, ReservationAdmin)
