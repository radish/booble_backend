from psycopg2.extras import DateTimeTZRange
from rest_framework.reverse import reverse
from rest_framework.test import APIRequestFactory
from rest_framework.test import APITestCase

from rooms.models import Reservation, Room, ReservationDateTimeRange
from users.models import User


class ReservationsTests(APITestCase):
    def setUp(self):
        self.factory = APIRequestFactory()

        self.user = User.objects.create(email='test@test.com', password='test')
        self.room = Room.objects.create(name='Test Room', capacity=5,
                                        address='Test', description='Test')
        reservation = Reservation.objects.create(owner=self.user,
                                                 room=self.room)
        ReservationDateTimeRange.objects.create(
            reservation=reservation,
            range=DateTimeTZRange('2016-01-01 14:00', '2016-01-01 17:00')
        )

    def test_creating_single_bookings(self):
        url = reverse('reservations-list')
        self.client.force_authenticate(self.user)

        with self.subTest('Exactly as existing'):
            data = {
                'room': self.room.id,
                'datetime_ranges': [
                    ['2016-01-01 14:00', '2016-01-01 17:00'],
                ]
            }

            response = self.client.post(url, data, format='json')
            self.assertEqual(response.status_code, 400)

        with self.subTest('Overlapping with existing'):
            data = {
                'room': self.room.id,
                'datetime_ranges': [
                    ['2016-01-01 13:00', '2016-01-01 15:00'],
                ]
            }

            response = self.client.post(url, data, format='json')
            self.assertEqual(response.status_code, 400)

        with self.subTest('Before existing'):
            data = {
                'room': self.room.id,
                'datetime_ranges': [
                    ['2016-01-01 13:00', '2016-01-01 14:00'],
                ]
            }

            response = self.client.post(url, data, format='json')
            self.assertEqual(response.status_code, 201)

        with self.subTest('After existing'):
            data = {
                'room': self.room.id,
                'datetime_ranges': [
                    ['2016-01-01 17:00', '2016-01-01 18:00'],
                ]
            }

            response = self.client.post(url, data, format='json')
            self.assertEqual(response.status_code, 201)

    def test_creating_multiple_bookings(self):
        url = reverse('reservations-list')
        self.client.force_authenticate(self.user)

        with self.subTest('Multiple at once'):
            data = {
                'room': self.room.id,
                'datetime_ranges': [
                    ['2016-01-01 18:00', '2016-01-01 19:00'],
                    ['2016-01-01 19:00', '2016-01-01 20:00'],
                    ['2016-01-02 19:00', '2016-01-02 20:00'],
                ]
            }

            response = self.client.post(url, data, format='json')
            self.assertEqual(response.status_code, 201)

        with self.subTest('Multiple at once with overlapping'):
            data = {
                'room': self.room.id,
                'datetime_ranges': [
                    ['2016-01-01 18:30', '2016-01-01 19:30'],
                    ['2016-01-01 22:00', '2016-01-01 23:00'],
                    ['2016-01-05 19:00', '2016-01-05 20:00'],
                ]
            }

            response = self.client.post(url, data, format='json')
            self.assertEqual(response.status_code, 400)
