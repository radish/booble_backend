from django.contrib.postgres import fields
from django.db import models

from users.models import User


class Room(models.Model):
    use_in_migrations = True

    name = models.CharField(max_length=64)
    capacity = models.PositiveIntegerField()
    address = models.CharField(max_length=128)
    description = models.TextField()

    def __str__(self):
        return self.name


class Reservation(models.Model):
    owner = models.ForeignKey(User, on_delete=models.CASCADE)
    room = models.ForeignKey(Room, on_delete=models.CASCADE)

    def __str__(self):
        return '{}: {}'.format(self.owner, self.room)


class ReservationDateTimeRange(models.Model):
    range = fields.DateTimeRangeField()
    reservation = models.ForeignKey(Reservation, on_delete=models.CASCADE)

    def __str__(self):
        return '{} - {}'.format(self.range.lower, self.range.upper)
