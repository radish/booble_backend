from django.core.management.base import BaseCommand

from rooms.models import Room


class Command(BaseCommand):
    help = 'Adds a specified number of rooms to the database'

    def add_arguments(self, parser):
        parser.add_argument('number', type=int)

    def handle(self, *args, **options):
        for i in range(1, options['number'] + 1):
            self.stdout.write('Creating Room {0}... '.format(i), ending='')

            r = Room(name='Room {0}'.format(i), capacity=10 + i,
                     address='Street {0}'.format(i),
                     description='Room {0} is a great room'.format(i))
            r.save()

            self.stdout.write('OK')
