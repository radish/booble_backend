from django.core.management.base import BaseCommand
from psycopg2.extras import DateTimeTZRange

from rooms.models import Reservation, Room, ReservationDateTimeRange
from users.models import User
from random import randint
from datetime import datetime, timedelta

class Command(BaseCommand):
    help = 'Adds a specified number of cyclic bookings to the database'

    def add_arguments(self, parser):
        parser.add_argument('number', type=int)

    def handle(self, *args, **options):
        owners = User.objects.all()
        ownersN = owners.count()
        rooms = Room.objects.all()
        roomsN = rooms.count()
        d=datetime.now()

        for i in range(1, options['number'] + 1):
            self.stdout.write('Creating Booking{0}... '.format(i), ending='')
            d = d.replace(month=randint(1,12))
            d = d.replace(day=randint(1,28))
            s_date = d
            e_date = d
            t = randint(1,12)
            s_date = s_date.replace(hour=t)
            e_date = e_date.replace(hour=t + randint(1,5))

            e_cyclicdate = e_date + timedelta(weeks=randint(1,13))
            period = 0
            period_chooser = randint(1,2)
            if period_chooser == 1:
                period = 7
            else:
                period = 14


            dateranges = []
            while e_date <= e_cyclicdate:
                dateranges.append(DateTimeTZRange(s_date, e_date))
                s_date = s_date + timedelta(days=period)
                e_date = e_date + timedelta(days=period)

            reservation = Reservation.objects.create(owner=owners[(i+t) % ownersN], room=rooms[(i+t) % roomsN])
            for daterange in dateranges:
                reservation2 = ReservationDateTimeRange.objects.create(
                    reservation=reservation,
                    range=daterange
                )
            reservation2.save()
            self.stdout.write('OK')
