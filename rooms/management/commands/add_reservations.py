from django.core.management.base import BaseCommand
from psycopg2.extras import DateTimeTZRange

from rooms.models import Reservation, Room, ReservationDateTimeRange
from users.models import User
from random import randint
import datetime

class Command(BaseCommand):
    help = 'Adds a specified number of bookings to the database'

    def add_arguments(self, parser):
        parser.add_argument('number', type=int)

    def handle(self, *args, **options):
        owners = User.objects.all()
        ownersN = owners.count()
        rooms = Room.objects.all()
        roomsN = rooms.count()
        d=datetime.datetime.now()

        for i in range(1, options['number'] + 1):
            self.stdout.write('Creating Booking{0}... '.format(i), ending='')
            d = d.replace(month=randint(1,12))
            d = d.replace(day=randint(1,28))
            s_date = d
            e_date = d
            t = randint(1,12)
            s_date = s_date.replace(hour=t)
            e_date = e_date.replace(hour=t + randint(1,5))
            daterange=DateTimeTZRange(s_date, e_date)

            reservation = Reservation.objects.create(owner=owners[(i+t) % ownersN], room=rooms[(i+t) % roomsN])
            reservation2 = ReservationDateTimeRange.objects.create(
                reservation=reservation,
                range=daterange
            )
            reservation2.save()
            self.stdout.write('OK')
