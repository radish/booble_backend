from django.core.management.base import BaseCommand

from rooms.models import CyclicBooking


class Command(BaseCommand):
    help = 'Deletes all rooms from the database'

    def handle(self, *args, **options):
        self.stdout.write('Deleting all cyclic bookings from the database... ', ending='')

        CyclicBooking.objects.all().delete()

        self.stdout.write('OK')
