from django.core.management.base import BaseCommand

from rooms.models import Booking


class Command(BaseCommand):
    help = 'Deletes all bookings from the database'

    def handle(self, *args, **options):
        self.stdout.write('Deleting all bookings from the database... ', ending='')

        Booking.objects.all().delete()

        self.stdout.write('OK')
