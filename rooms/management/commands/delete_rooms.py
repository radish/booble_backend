from django.core.management.base import BaseCommand

from rooms.models import Room


class Command(BaseCommand):
    help = 'Deletes all rooms from the database'

    def handle(self, *args, **options):
        self.stdout.write('Deleting all rooms from the database... ', ending='')

        Room.objects.all().delete()

        self.stdout.write('OK')
