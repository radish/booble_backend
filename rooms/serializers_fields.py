from django.utils.translation import ugettext as _
from psycopg2.extras import DateTimeTZRange
from rest_framework import serializers


class DateTimeRangeField(serializers.RelatedField, serializers.DateTimeField):
    default_error_messages = {
        'invalid_length': _('Expected a list containing two values but got '
                            '{type} containing {length} elements.'),
        'invalid': _('Datetime in range has wrong format. '
                     'Use one of these formats instead: {format}.'),
        'date': _('Expected a datetime range but got a date range.'),
    }

    def to_internal_value(self, value):
        if not isinstance(value, list) or len(value) != 2:
            self.fail('invalid_length', type=type(value), length=len(value))

        start_datetime, end_datetime = value

        return DateTimeTZRange(super().to_internal_value(start_datetime),
                               super().to_internal_value(end_datetime))

    def to_representation(self, value):
        if not value:
            return None

        start_datetime, end_datetime = value.range.lower, value.range.upper

        return (super().to_representation(start_datetime),
                super().to_representation(end_datetime))

