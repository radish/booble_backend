from rest_framework import generics
from rest_framework.permissions import AllowAny, IsAuthenticated

from rooms.models import Room, Reservation
from rooms.serializers import RoomSerializer, ReservationDetailedSerializer


class ListRooms(generics.ListAPIView):
    queryset = Room.objects.all()
    serializer_class = RoomSerializer
    permission_classes = (AllowAny,)


class RoomDetails(generics.RetrieveAPIView):
    queryset = Room.objects.all()
    serializer_class = RoomSerializer
    permission_classes = (AllowAny,)


class ReservationsList(generics.ListCreateAPIView):
    serializer_class = ReservationDetailedSerializer
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        return Reservation.objects.filter(
            owner=self.request.user
        ).prefetch_related('reservationdatetimerange_set')

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)

class ReservationsDetails(generics.DestroyAPIView):
    serializer_class = ReservationDetailedSerializer
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        return Reservation.objects.filter(
            owner=self.request.user
        ).prefetch_related('reservationdatetimerange_set')
