# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2016-04-29 20:36
from __future__ import unicode_literals

from django.conf import settings
import django.contrib.postgres.fields.ranges
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    replaces = [('rooms', '0003_auto_20160429_1447'), ('rooms', '0004_auto_20160429_1818'), ('rooms', '0005_auto_20160429_2028')]

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('rooms', '0002_auto_20160412_2005'),
    ]

    operations = [
        migrations.CreateModel(
            name='Reservation',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('owner', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
                ('room', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='rooms.Room')),
            ],
        ),
        migrations.RemoveField(
            model_name='booking',
            name='owner',
        ),
        migrations.RemoveField(
            model_name='booking',
            name='room',
        ),
        migrations.RemoveField(
            model_name='cyclicbooking',
            name='owner',
        ),
        migrations.RemoveField(
            model_name='cyclicbooking',
            name='room',
        ),
        migrations.DeleteModel(
            name='Booking',
        ),
        migrations.DeleteModel(
            name='CyclicBooking',
        ),
        migrations.CreateModel(
            name='ReservationDateTimeRange',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('range', django.contrib.postgres.fields.ranges.DateTimeRangeField()),
                ('reservation', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='rooms.Reservation')),
            ],
        ),
    ]
