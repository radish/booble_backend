import operator
from functools import reduce

from django.db import transaction
from django.db.models import Q
from django.utils.translation import ugettext as _
from rest_framework import serializers
from rest_framework.fields import CurrentUserDefault

from rooms.models import Room, Reservation, ReservationDateTimeRange
from rooms.serializers_fields import DateTimeRangeField


class RoomSerializer(serializers.ModelSerializer):
    class Meta:
        model = Room
        fields = ('id', 'name', 'capacity', 'address', 'description',)
        read_only_fields = ('id',)


class ReservationDetailedSerializer(serializers.ModelSerializer):
    datetime_ranges = DateTimeRangeField(
        source='reservationdatetimerange_set', many=True,
        queryset=ReservationDateTimeRange.objects.all()
    )

    class Meta:
        model = Reservation
        fields = ('id', 'room', 'datetime_ranges',)
        read_only_fields = ('id',)

    def create(self, validated_data):
        owner = None
        request = self.context.get('request')
        if request and hasattr(request, 'user'):
            owner = request.user

        with transaction.atomic():
            reservation = Reservation.objects.create(
                owner=owner,
                room=validated_data['room']
            )

            for dt_range in validated_data['reservationdatetimerange_set']:
                ReservationDateTimeRange.objects.create(reservation=reservation,
                                                        range=dt_range)
            return reservation

    def update(self, instance, validated_data):
        print('UPDATE WAT')

    def validate(self, attrs):
        is_invalid = Reservation.objects.filter(
            Q(room=attrs['room']),
            reduce(
                operator.or_, (
                    Q(reservationdatetimerange__range__overlap=datetime_range)
                    for datetime_range in attrs['reservationdatetimerange_set']
                )
            )
        ).exists()

        if is_invalid:
            raise serializers.ValidationError(_('Room is unavailable at '
                                                'requested time'))

        return attrs
