from django.conf.urls import url

from . import views

urlpatterns = [
    # url(r'^$', views.UserList.as_view()),
    url(r'^register$', views.UserRegister.as_view()),
    url(r'^login$', views.UserLogin.as_view()),
    url(r'^logout$', views.UserLogout.as_view()),
]
