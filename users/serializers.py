from django.contrib.auth import get_user_model, authenticate
from django.core.exceptions import ObjectDoesNotExist
from django.utils.translation import ugettext as _
from rest_framework import serializers
from rest_framework.serializers import ValidationError

from users.models import User


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = get_user_model()
        fields = ('email', 'password', 'first_name', 'last_name',)
        extra_kwargs = {'password': {'write_only': True}}
        read_only_fields = ('is_staff', 'is_superuser', 'is_active',
                            'date_joined',)

    def create(self, validated_data):
        user = get_user_model()(
            email=validated_data['email'],
            first_name=validated_data['first_name'],
            last_name=validated_data['last_name']
        )
        user.set_password(validated_data['password'])
        user.save()
        return user


class LoginSerializer(serializers.Serializer):
    email = serializers.EmailField()
    password = serializers.CharField(max_length=128, write_only=True)

    def validate_email(self, value):
        user_model = get_user_model()  # type: User
        try:
            self.__email = user_model.objects.get(email=value).email
        except ObjectDoesNotExist:
            raise ValidationError(_('Email does not exist in database.'))

    def validate_password(self, value):
        try:
            self.__user = authenticate(email=self.__email, password=value)

            if not self.__user:
                raise ValidationError(_('Invalid password.'))
        except AttributeError:
            # Email validation failed
            pass

    def validate(self, attrs):
        try:
            if not self.__user.is_active:
                raise ValidationError(_('User is inactive.'))

            attrs['user'] = self.__user
            return attrs
        except AttributeError:
            # Email and/or password validation failed
            return attrs
