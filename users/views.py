from django.contrib.auth import get_user_model
from rest_framework import generics
from rest_framework.authtoken.models import Token
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from users.permissions import IsNotAuthenticated
from users.serializers import UserSerializer, LoginSerializer


class UserRegister(generics.CreateAPIView):
    serializer_class = UserSerializer
    queryset = get_user_model().objects.all()

    permission_classes = (IsNotAuthenticated,)


class UserLogin(APIView):
    serializer_class = LoginSerializer

    permission_classes = (IsNotAuthenticated,)

    def post(self, request, format=None):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']
        token, created = Token.objects.get_or_create(user=user)
        return Response({'token': token.key})


class UserLogout(APIView):
    permission_classes = (IsAuthenticated,)

    def post(self, request, format=None):
        Token.objects.filter(user=request.user).delete()
        return Response()
