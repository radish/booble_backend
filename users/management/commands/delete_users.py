from django.core.management.base import BaseCommand

from users.models import User


class Command(BaseCommand):
    help = 'Deletes all non-staff users from the database'

    def handle(self, *args, **options):
        self.stdout.write('Deleting non-staff users from the database... ',
                          ending='')

        User.objects.filter(is_staff=False).delete()

        self.stdout.write('OK')
