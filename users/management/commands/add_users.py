from django.core.management.base import BaseCommand

from users.models import User


class Command(BaseCommand):
    help = 'Adds a specified number of users to the database'

    def add_arguments(self, parser):
        parser.add_argument('number', type=int)

    def handle(self, *args, **options):
        for i in range(1, options['number'] + 1):
            self.stdout.write('Creating user{0}... '.format(i), ending='')

            r = User(email='user{0}'.format(i) + '@user.pl',
                     first_name='User {0}'.format(i), last_name='Userowicz',
                     date_of_birth='2000-01-01', )
            r.set_password('user{0}'.format(i))
            r.save()

            self.stdout.write('OK')
